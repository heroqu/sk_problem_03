function longest(str) {
  const reader = makeReader()
  for (let c of str) {
    reader.read(c)
  }
  return reader.longest()
}

function Sequence() {
  return {
    first: '',
    value: '',
    s: new Set(),
    size() {
      return this.value.length
    },
    isAddable(char) {
      return this.s.size < 2 || this.s.has(char)
    },
    add(char) {
      if (char) {
        if (!this.first) {
          this.first = char
        }
        this.value += char
        this.s.add(char)
      }
      return this.size()
    }
  }
}

function makeReader() {
  return {
    lastChar: '',
    seq1: Sequence(),
    seq2: Sequence(),
    best: Sequence(),
    longest: function() {
      console.log(this.best.value)
      return this.best.value
    },
    newSequence: function(char, otherSequence) {
      const seq = Sequence()
      // символы, с которых должна начинаться новая последовательность
      let chars = [this.lastChar, char].filter(Boolean)
      // эта последовательность не должна начинать с того же
      // символа, с которого уже начинается другая
      while (otherSequence.first && chars[0] === otherSequence.first) {
        chars = chars.slice(1)
      }
      for (let c of chars) {
        seq.add(c)
      }
      return seq
    },
    read: function(char) {
      if (this.seq1.isAddable(char)) {
        if (this.seq1.add(char) > this.best.size()) {
          this.best = this.seq1
        }
      } else {
        // последовательность оборвалась, начинаем новую
        this.seq1 = this.newSequence(char, this.seq2)
      }
      if (this.seq2.isAddable(char)) {
        if (this.seq2.add(char) > this.best.size()) {
          this.best = this.seq2
        }
      } else {
        // последовательность оборвалась, начинаем новую
        this.seq2 = this.newSequence(char, this.seq1)
      }
      this.lastChar = char
    }
  }
}

module.exports = longest
