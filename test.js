const assert = require('assert')
const longest = require('./longest')

describe('Finding longest 2-char substring', () => {
  it('should correctly find in case 1', () => {
    assert.equal(longest('abc efgh'), 'ab')
  })

  it('should correctly find in case 2', () => {
    assert.equal(longest('listen abba sings'), 'abba')
  })

  it('should correctly find in case 3', () => {
    assert.equal(longest('abba sings lalalalala...'), 'lalalalala')
  })

  it('should be case sensitive', () => {
    assert.equal(longest('abba is longer then aBabaBa'), 'abba')
  })

  it('should treat spaces as chars', () => {
    assert.equal(longest('abba is smaller then space:     '), ':     ')
  })

  it('should return the first out of two equaly long substrings', () => {
    assert.equal(longest('The abba supersedes ABBA'), 'abba')
  })
})
